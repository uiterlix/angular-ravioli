'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('RavioliCtrl', ['$scope', '$window', '$http','ravioliService', function($scope, $window, $http, ravioliService) {

  		$scope.username = $window.username;
  		$scope.mouse = null;
  		$scope.components = [];
  		$scope.selectedItems = [];
		$scope.selection = [];
		$scope.updater = null;
		
  		var unbind = $window.modelWatch.watch(function(graphModel) {
  			$scope.components = graphModel;
			$scope.updater = 'trigger';
  			$scope.$apply()
			$scope.updater = null;
			var sel = selections();
			setFilter(sel);				
  		});
		
  		$scope.$watch('components', function() {
			if ($scope.updater === null) {
				$window.fireGraphUpdate();
			}
  		}, true);
		
  		// Unbind the listener when the scope is destroyed
        $scope.$on('$destroy', unbind);
		
		// $scope.$on('$viewContentLoaded', $scope.readyFunction);
		$scope.$on('$viewContentLoaded', function () {
			$scope.readyFunction();
		} );		
		
		$scope.readyFunction = function() {
			console.log('READY!');		
		};		

  		$scope.selectItem = function(component, index) {
  			// slight hack for not so tidy Json
  			/*
  		var theKey = null;
  		var i = 0;
  		for (var key in $scope.components) {
  			console.log('eval: ' + key);
  			if (i == index) {
  				theKey = key;
  				break;
  			}
  			i = i + 1;
  		}
  		*/
  			var theComponent = $scope.components[index];
  			$scope.selectedItem = theComponent.identifier + ', visible: ' + component.visible;
  			$scope.selectedIndex = index;
  		};
		
		$scope.toggleVisibility = function(component, index) {
  			var theComponent = $scope.components[index];
			$window.fireGraphUpdate();
		}

  		$scope.loadComponentModel = function() {
  			ravioliService.load($scope.loadComponentModelCallback);
  		};

  		$scope.loadComponentModelCallback = function(data) { //, status, headers, config
  			$scope.components = data;
  		};

  		$scope.clearComponentModel = function() {
  			$scope.components = [];
  			$scope.selectedItem = null;
  		};
		
  		$scope.mouseOver = function(index) {
  			if ($scope.mouse === 'down') {
  				$scope.selectedItems.push(index);
  			}
  		};
		
  		$scope.mouseDown = function() {
  			$scope.selectedItems = [];
  			console.log('down');
  			$scope.mouse = 'down';
  		};
		
  		$scope.mouseUp = function() {
  			console.log('up');			
  			$scope.mouse = 'up';
  		};
		
  		$scope.mouseEnter = function() {
  			console.log('enter');
  			$scope.mouse = 'up';
  		};
		
  		$scope.mouseLeave = function() {
  			console.log('leave');			
  			$scope.mouse = 'up';
  		};	
		
		/*
		Selection functions
		*/
		var selectItems = function(items, firstidx, lastidx, selectedItems) {
			var idx = firstidx;
			while (idx < lastidx + 1) {
				if (items[idx] && $.inArray(idx, selectedItems) === -1) {
					selectItem(items[idx]);
                    selectedItems.push(idx);
				}
				idx++;
			}
		}

		var selectItem = function(item) {
			if (item.className.indexOf('checked') > -1) {
				item.className = item.className.replace(' checked', '');
			} else {
				item.className = item.className + ' checked';
			}
		}

		var selections = function() {
			var mouseIsDown = false;
			var prevItem;
            var selectedItems = [];

			$(window).on('mousedown.selection', function(e) { 
				mouseIsDown = true;
			}).on('mouseup.selection', function(e) { 
				mouseIsDown = false;
				prevItem = undefined;
                selectedItems = [];
			});

			var allItems = $('#optionlist li span');
			allItems.click(function(e) {
				selectItem(this);
			});

			$('#optionlist li span').mouseenter(function(e) {
				if (e.target.localName !== 'span') {
					return;
				}
				if (!mouseIsDown) { return; }
				if (e.target !== prevItem) {
                    if (!prevItem) {
                        prevItem = e.target;
                    }
					var firstIndex = $.inArray(prevItem, allItems) - 1;
					var lastIndex = $.inArray(e.target, allItems);

                    if (firstIndex > -1 && lastIndex > -1) {
                        if (firstIndex < lastIndex) {
                            selectItems(allItems, firstIndex, lastIndex, selectedItems);
                        } else {
                            selectItems(allItems, lastIndex, firstIndex, selectedItems);
                        }
                    }
				}
				prevItem = e.target;
			});

			var resetAllItems = function() {
				allItems = $('#optionlist li span');
			};

			return {
				"resetAllItems": resetAllItems
			};
		}

		var setFilter = function(sel) {
			$('#applyfilter').click(function() {
				var filtertext = $('#filter').val();
				$('#optionlist li').show().not('li:contains(' + filtertext + ')').hide();
				sel.resetAllItems();
			});
		}		
		/*
		End of selection functions
		*/

  	}]);
  