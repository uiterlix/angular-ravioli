'use strict';

/* Services */
angular.module('myApp.services', [])
  	.factory('ravioliService', ['$http', function($http) {
  			// AngularJS will instantiate a singleton by calling "new" on this function
  			var loadComponents = function(callback) {
  				$http.get('http://localhost:8080/ravioli-service/list').success(callback);
  			};

  			return {
  				load: function(callback) {
  					return loadComponents(callback);
  				}
  			};
  		}
  	]);
  
